import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.lang.reflect.Method;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.JSONObject;
//import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.internal.path.json.mapping.JsonObjectDeserializer;
import junit.framework.Assert;



public class Post {
	
	@Test(dataProvider="ordersData") 
	//Post 
	void addNewOrders(String ecrust, String eflavor,String eorder_id, String esize, String etable_no, String etimetamp)

	{
	RestAssured.baseURI="https://order-pizza-api.herokuapp.com/api";

	RequestSpecification httpRequest = RestAssured.given();

	// we created data that can be sent with post request

	JSONObject requestParams = new JSONObject();

	requestParams.put("Crust", ecrust);
	requestParams.put("Flavor", eflavor);
	requestParams.put("Order_ID", eorder_id);
	requestParams.put("Size",esize);
	requestParams.put("Table_No", etable_no);
	requestParams.put("Timestamp", etimetamp);

	// add header
	httpRequest.header("content-type","application/json");
	httpRequest.header("Accept", "application/json");

	// add json to the body
	httpRequest.body(requestParams.toString());

	// post request

	Response response=httpRequest.request(Method.POST,"/orders");

	//capture response body
	String responseBody= response.getBody().asString();

	Assert.assertEquals(responseBody.contains(ecrust),true);
	Assert.assertEquals(responseBody.contains(eflavor),true);
	Assert.assertEquals(responseBody.contains(eorder_id),true);
	Assert.assertEquals(responseBody.contains(esize),true);
	Assert.assertEquals(responseBody.contains(etable_no),true);
	Assert.assertEquals(responseBody.contains( etimetamp),true);

	int status =response.getStatusCode();

	Assert.assertEquals(status,200);


	}
	
	
// data provider method
	@DataProvider(name="ordersData")
	// read from excel file
	
	String [][] getOrdersData()
	
	{
		String path="/src/post.xlsx";

		int row= XlUtils.getRowCount(path, "Sheet1");
		int col= XlUtils.getCellCount(path, "Sheet1", 1);
		
				String ordersData[][]=new String[row][col];
				
		for(int i=1; i<=row;i++)
		{
			for(int j=0;j<=col;j++)
			{
				ordersData[i+1][j]=XlUtils.getCellData(path, "Sheet1", i, j);
			}
		}
		
		
		
		
		return (ordersData);
		
	}
	
	
	
}